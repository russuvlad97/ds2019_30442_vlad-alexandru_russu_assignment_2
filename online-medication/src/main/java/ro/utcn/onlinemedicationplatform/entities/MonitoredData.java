package ro.utcn.onlinemedicationplatform.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import ro.utcn.onlinemedicationplatform.entities.util.LocalDateTimeSerializer;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MonitoredData {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Long id;

    @Column
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime startTime;

    @Column
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime endTime;

    @Column
    private String activity;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "patient_id")
    private Patient patient;

    private Long pId;

    public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activity){
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public Integer getDuration() {
        return (int) Duration.between(this.startTime, this.endTime).toHours();
    }

    @Override
    public String toString() {
        return "MonitoredData{" +
                "id=" + id +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", activity='" + activity + '\'' +
                ", patient=" + patient +
                ", pId=" + pId +
                '}';
    }
}
