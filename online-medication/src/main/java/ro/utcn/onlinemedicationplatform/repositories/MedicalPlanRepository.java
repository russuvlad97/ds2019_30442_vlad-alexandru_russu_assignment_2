package ro.utcn.onlinemedicationplatform.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.utcn.onlinemedicationplatform.dto.MedicalPlanDTO;
import ro.utcn.onlinemedicationplatform.entities.MedicalPlan;
import ro.utcn.onlinemedicationplatform.entities.Patient;

import java.util.Optional;

@Repository
public interface MedicalPlanRepository extends JpaRepository<MedicalPlan, Long> {

    Optional<MedicalPlan> findByPatient(Patient patient);

}
