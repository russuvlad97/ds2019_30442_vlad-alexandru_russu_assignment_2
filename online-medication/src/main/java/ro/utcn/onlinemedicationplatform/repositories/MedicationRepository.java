package ro.utcn.onlinemedicationplatform.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.utcn.onlinemedicationplatform.entities.Medication;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Long> {

}
