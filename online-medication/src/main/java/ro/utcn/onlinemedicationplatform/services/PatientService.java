package ro.utcn.onlinemedicationplatform.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcn.onlinemedicationplatform.dto.PatientDTO;
import ro.utcn.onlinemedicationplatform.dto.builders.PatientBuilder;
import ro.utcn.onlinemedicationplatform.dto.builders.PatientViewBuilder;
import ro.utcn.onlinemedicationplatform.dto.view.PatientViewDTO;
import ro.utcn.onlinemedicationplatform.entities.Patient;
import ro.utcn.onlinemedicationplatform.errorhandler.ResourceNotFoundException;
import ro.utcn.onlinemedicationplatform.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public PatientDTO findPatientById(Long id){
        Optional<Patient> patient = patientRepository.findById(id);
        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient", "patient_id", id);
        }
        return PatientBuilder.generateDTOFromEntity(patient.get());
    }

    public Patient findPatientByPId(Long id){
        Optional<Patient> patient = patientRepository.findById(id);
        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient", "patient_id", id);
        }
        return patient.get();
    }

    public PatientDTO findPatientByUsername(String patientname){
        Optional<Patient> patient = patientRepository.findByUsername(patientname);
        if(!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "patient_patientname", patientname);
        }
        return PatientBuilder.generateDTOFromEntity(patient.get());

    }

    public List<PatientDTO> findAllPatients(){
        List<Patient> patients = patientRepository.findAll();
        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Long insert(PatientDTO patientDTO){
        //TODO validator
        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
                .getId();
    }

    public Long update(Long id, PatientDTO patientDTO){
        Optional<Patient> doctor = patientRepository.findById(id);
        if(!doctor.isPresent()){
            throw new ResourceNotFoundException("Patient", "patient_id", patientDTO.getId());
        }
        this.patientRepository.deleteById(id);
        //TODO validator
        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
                .getId();
    }

    public void delete(Long id) {
        this.patientRepository.deleteById(id);
    }

    public Long login(String username, String password){
        Patient patient = patientRepository.findByUsername(username).get();
        if(patient != null){
            if(patient.getPassword().equals(password)){
                return patient.getId();
            }
        }
        return Long.parseLong("-1");
    }
}
