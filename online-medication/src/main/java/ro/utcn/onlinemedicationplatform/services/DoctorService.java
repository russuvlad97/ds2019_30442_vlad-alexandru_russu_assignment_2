package ro.utcn.onlinemedicationplatform.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcn.onlinemedicationplatform.dto.DoctorDTO;
import ro.utcn.onlinemedicationplatform.dto.MedicalPlanDTO;
import ro.utcn.onlinemedicationplatform.dto.builders.DoctorBuilder;
import ro.utcn.onlinemedicationplatform.dto.builders.DoctorViewBuilder;
import ro.utcn.onlinemedicationplatform.dto.view.DoctorViewDTO;
import ro.utcn.onlinemedicationplatform.entities.Doctor;
import ro.utcn.onlinemedicationplatform.errorhandler.ResourceNotFoundException;
import ro.utcn.onlinemedicationplatform.repositories.DoctorRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public DoctorViewDTO findDoctorById(Long id){
        Optional<Doctor> doctor = doctorRepository.findById(id);
        if(!doctor.isPresent()){
            throw new ResourceNotFoundException("Doctor", "doctor_id", id);
        }
        return DoctorViewBuilder.generateDTOFromEntity(doctor.get());
    }

    public List<DoctorViewDTO> findAll(){
        List<Doctor> doctors = doctorRepository.findAll();
        return doctors.stream()
                .map(DoctorViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Long insert(DoctorDTO doctorDTO){
        // TODO validator
        return doctorRepository
                .save(DoctorBuilder.generateEntityFromDTO(doctorDTO))
                .getId();
    }

    public Long update(DoctorDTO doctorDTO){
        Optional<Doctor> doctor = doctorRepository.findById(doctorDTO.getId());
        if(!doctor.isPresent()){
            throw new ResourceNotFoundException("Doctor", "doctor_id", doctorDTO.getId());
        }
        //TODO validator
        return doctorRepository
                .save(DoctorBuilder.generateEntityFromDTO(doctorDTO))
                .getId();
    }

    public Long login(String username, String password){
        Doctor doctor = doctorRepository.findByUsername(username).get();
        if(doctor != null){
            if(doctor.getPassword().equals(password)){
                return doctor.getId();
            }
        }
        return Long.parseLong("-1");
    }

}
