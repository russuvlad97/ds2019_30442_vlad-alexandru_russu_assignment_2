package ro.utcn.onlinemedicationplatform.dto.builders;

import lombok.NoArgsConstructor;
import ro.utcn.onlinemedicationplatform.dto.MedicationDTO;
import ro.utcn.onlinemedicationplatform.entities.Medication;

@NoArgsConstructor
public class MedicationBuilder {

    public static MedicationDTO generateDTOFromEntity(Medication medication){
        return new MedicationDTO(
                medication.getId(),
                medication.getName(),
                medication.getSideEffects(),
                medication.getDosage()
        );
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO){
        return new Medication(
                medicationDTO.getName(),
                medicationDTO.getSideEffects(),
                medicationDTO.getDosage()
        );
    }

}
