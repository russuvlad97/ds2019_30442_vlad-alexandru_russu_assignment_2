package ro.utcn.onlinemedicationplatform.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ro.utcn.onlinemedicationplatform.entities.util.Type;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private Long id;
    private String name;
    private String username;
    private String password;
    private Type type;
    private String birthDate;
    private Character gender;
    private String address;
}
