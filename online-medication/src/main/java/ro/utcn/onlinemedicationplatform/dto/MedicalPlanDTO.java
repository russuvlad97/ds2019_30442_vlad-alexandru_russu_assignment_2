package ro.utcn.onlinemedicationplatform.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ro.utcn.onlinemedicationplatform.entities.Medication;
import ro.utcn.onlinemedicationplatform.entities.Patient;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MedicalPlanDTO {

    private Long id;
    private String name;
    private String instructions;
    private String treatmentPeriod;
    private List<Medication> medications;
    private Patient patient;

}
