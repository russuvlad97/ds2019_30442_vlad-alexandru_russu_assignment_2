package ro.utcn.onlinemedicationplatform.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MedicationDTO {

    private Long id;
    private String name;
    private String sideEffects;
    private String dosage;

}
