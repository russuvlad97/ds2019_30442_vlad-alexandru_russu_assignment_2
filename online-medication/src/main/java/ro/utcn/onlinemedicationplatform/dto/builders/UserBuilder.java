package ro.utcn.onlinemedicationplatform.dto.builders;

import lombok.NoArgsConstructor;
import ro.utcn.onlinemedicationplatform.dto.UserDTO;
import ro.utcn.onlinemedicationplatform.entities.User;

@NoArgsConstructor
public class UserBuilder {

    public static UserDTO generateDTOFromEntity(User user){
        return new UserDTO(
                user.getId(),
                user.getName(),
                user.getUsername(),
                user.getPassword(),
                user.getType(),
                user.getBirth_date(),
                user.getGender(),
                user.getAddress()
        );
    }

    public static User generateEntityFromDTO(UserDTO userDTO){
        return new User(
                userDTO.getName(),
                userDTO.getUsername(),
                userDTO.getPassword(),
                userDTO.getType(),
                userDTO.getBirthDate(),
                userDTO.getGender(),
                userDTO.getAddress()
        );
    }

}
