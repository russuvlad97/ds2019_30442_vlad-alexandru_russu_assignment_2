package ro.utcn.onlinemedicationplatform.controllers;

import org.springframework.web.bind.annotation.*;
import ro.utcn.onlinemedicationplatform.services.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/login")
public class LoginController {

    private final PatientService patientService;
    private final CaregiverService caregiverService;
    private final DoctorService doctorService;

    public LoginController(PatientService patientService, CaregiverService caregiverService, DoctorService doctorService) {
        this.patientService = patientService;
        this.caregiverService = caregiverService;
        this.doctorService = doctorService;
    }


    @RequestMapping(value = "/{username}/{password}/{type}", method = RequestMethod.GET)
    public Long login(@PathVariable("username") String username, @PathVariable("password") String password, @PathVariable("type") String type) {
        System.out.println("salut");
        switch (type){
            case "patient":
                return patientService.login(username, password);
            case "caregiver":
                return caregiverService.login(username, password);
            default:
                return doctorService.login(username, password);
        }

    }
}
