package ro.utcn.onlinemedicationplatform.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.utcn.onlinemedicationplatform.dto.MedicationDTO;
import ro.utcn.onlinemedicationplatform.dto.view.MedicationViewDTO;
import ro.utcn.onlinemedicationplatform.services.MedicationService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value="/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public MedicationViewDTO findById(@PathVariable("id") Long id){
        return medicationService.findMedicationById(id);
    }

    @RequestMapping(value="/all", method = RequestMethod.GET)
    public List<MedicationViewDTO> findAllMedications(){
        return medicationService.findAllMedications();
    }

    @RequestMapping(value="/insert", method = RequestMethod.POST)
    public Long insert(@RequestBody MedicationDTO medicationDTO) { return medicationService.insert(medicationDTO); }
}
