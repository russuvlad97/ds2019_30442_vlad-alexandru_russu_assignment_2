package ro.utcn.onlinemedicationplatform;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import ro.utcn.onlinemedicationplatform.dto.MonitoredDataDTO;
import ro.utcn.onlinemedicationplatform.dto.builders.MonitoredDataBuilder;
import ro.utcn.onlinemedicationplatform.entities.MonitoredData;
import ro.utcn.onlinemedicationplatform.entities.Patient;
import ro.utcn.onlinemedicationplatform.services.MonitoredDataService;
import ro.utcn.onlinemedicationplatform.services.PatientService;

@Service
public class Recv {

    private final static String QUEUE_NAME = "broker";

    private static ObjectMapper objectMapper = new ObjectMapper();

    private final MonitoredDataService monitoredDataService;

    private final PatientService patientService;

    public Recv(MonitoredDataService monitoredDataService, PatientService patientService) {
        this.monitoredDataService = monitoredDataService;
        this.patientService = patientService;
    }

    @RabbitListener(queues = QUEUE_NAME)
    public void receiveMessage() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            objectMapper.registerModule(new JavaTimeModule());
            MonitoredDataDTO monitoredDataDTO = objectMapper.readValue(message, MonitoredDataDTO.class);
            Patient patient = patientService.findPatientByPId(monitoredDataDTO.getPId());
            MonitoredData monitoredData = MonitoredDataBuilder.generateEntityFromDTO(monitoredDataDTO);
//            monitoredData.setPatient(patient);
//            patient.getMonitoredData().add(monitoredData);
            monitoredDataService.insert(monitoredData);

            Integer duration = monitoredData.getDuration();
            System.out.println("+++++++++++++++++++++++ " + duration + " +++++++++++++++++++++++");

            switch(monitoredData.getActivity()){
                case "Sleeping":
                    if(duration>Integer.valueOf(12)){
                        System.out.println("NOTIFICATION: The patient has slept for more than 12 hours!");
                    }
                    break;
                case "Toileting":
                case "Showering":
                case "Grooming":
                    if(duration>Integer.valueOf(1)){
                        System.out.println("NOTIFICATION: The patient has been in the bathroom for more than 1 hour!");
                    }
                    break;
                case "Leaving":
                    if(duration>Integer.valueOf(12)){
                        System.out.println("NOTIFICATION: The patient has been outside for more than 12 hours!");
                    }
                    break;
            }

            System.out.println(monitoredData);
            System.out.println();
            System.out.println();
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }
}