package ro.utcn.producer.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ro.utcn.producer.model.util.*;

import java.time.LocalDateTime;

public class MonitoredData {

    private Long id;

    private Long pId;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime startTime;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime endTime;

    private String activity;

    public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activity, Long pId) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
        this.pId = pId;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }
    public LocalDateTime getEndTime() {
        return endTime;
    }
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
    public String getActivity() {
        return activity;
    }
    public void setActivity(String activity) {
        this.activity = activity;
    }
    public Long getPId() {
        return pId;
    }
    public void setPId(Long pId) {
        this.pId = pId;
    }
    /*
    public String getDate() {
        return startTime.getYear() + "-" + startTime.getMonthValue() + "-" + startTime.getDayOfMonth();
    }
    public Integer getDay() {
        return startTime.getDayOfMonth();
    }
    public Integer getDuration() {
        return (int) Duration.between(this.startTime, this.endTime).toMinutes();
    }
     */
}
