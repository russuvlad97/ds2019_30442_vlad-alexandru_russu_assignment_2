package ro.utcn.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ro.utcn.producer.model.MonitoredData;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Send {

    private final static String QUEUE_NAME = "broker";

    private final static String FILE_NAME = "activities.txt";

    private static int count = 0;

    private static List<MonitoredData> getLines(){
        try {
            return Files.lines(Paths.get(FILE_NAME))
                    .map(s->s.split("		"))
                    .map(s->{
                        s[0] = s[0].replace(' ', 'T').trim();
                        s[1] = s[1].replace(' ', 'T').trim();
                        s[2] = s[2].trim();
                        MonitoredData temp = new MonitoredData(LocalDateTime.parse(s[0]), LocalDateTime.parse(s[1]), s[2], Long.valueOf(1));
                        return temp;
                    })
                    .collect(Collectors.toList());
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Scheduled(fixedDelay = 1000)
    public void sendMessage() throws Exception {
        List<MonitoredData> monitoredData = getLines();
        ObjectMapper objectMapper = new ObjectMapper();

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = objectMapper.writeValueAsString(monitoredData.get(count));
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + message + "'");
            count++;
        }
    }
}
